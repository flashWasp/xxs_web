import { asyncRouterMap, constantRouterMap } from '@/router'

/**
 * 通过url判断是否与当前用户权限匹配
 * @param menuMap
 * @param route
 * @param path
 */
// function hasPermission(menuMap, route, path) {
//   if (route.role === 'admin') {
//     menuMap[path] = true
//   }
//   return !!menuMap[path]
// }

/**
 * 递归过滤异步路由表，返回符合用户角色权限的路由表
 * @param asyncRouterMap
 * @param menuMap
 * @param parentPath
 * @param notAccessPath
 */
// function filterAsyncRouter(asyncRouterMap, menuMap, parentPath = '', notAccessPath) {
//   const routers = asyncRouterMap.filter(route => {
//     let rs = false
//     const fullPath = parentPath + (parentPath ? '/' : '') + route.path
//     if (hasPermission(menuMap, route, fullPath)) {
//       rs = true
//     }
//     if (route.children && route.children.length > 0) {
//       route.children = filterAsyncRouter(route.children, menuMap, route.path, notAccessPath)
//       if (!rs && route.children.length > 0) {
//         rs = true
//       }
//     }
//     if (!rs) {
//       notAccessPath[fullPath] = true
//     }

//     if (rs && route.children && route.children.length > 0) {
//       const list = route.children
//       let flag = false
//       for (let i = 0; i < list.length; i++) {
//         const child = list[i]
//         if (!child.hidden) {
//           flag = true
//         }
//       }
//       if (!flag) {
//         route.hidden = true
//       }
//     }
//     return rs
//   })
//   return routers
// }

const permission = {
  state: {
    routers: constantRouterMap,
    addRouters: [],
    accessedPathMap: {},
    notAccessPathMap: {},
    buttonMap: {}
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      state.routers = constantRouterMap.concat(routers)
    },
    SET_ACCESSED_PATH_MAP: (state, accessedPathMap) => {
      state.accessedPathMap = accessedPathMap
    },
    SET_NOT_ACCESS_PATH_MAP: (state, notAccessPathMap) => {
      state.notAccessPathMap = notAccessPathMap
    },
    SET_BUTTON_MAP: (state, buttonMap) => {
      state.buttonMap = buttonMap
    }
  },
  actions: {
    GenerateRoutes({ commit }) {
      const menuMap = {}
      constantRouterMap.concat(asyncRouterMap).forEach(menu => {
        if (menu.children) {
          menu.children.forEach(childMenu => {
            menuMap[menu.path + (menu.path === '/' ? '' : '/') + childMenu.path] = true
          })
        }
        menuMap[menu.path] = true
      })
      commit('SET_ROUTERS', asyncRouterMap)
      commit('SET_ACCESSED_PATH_MAP', menuMap)
      // commit('SET_NOT_ACCESS_PATH_MAP', notAccessPath)
    }
  }
}

export default permission
