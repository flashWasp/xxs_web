'use strict'
import fetch from '@/utils/fetch'
import qs from 'qs'

export function loginByUsername(Account, Password) {
  return fetch({
    url: '/system/login',
    method: 'post',
    data: qs.stringify({
      Account,
      Password
    })
  })
}

export function valLogin(Account, Password) {
  return fetch({
    url: '/system/valLogin',
    method: 'post',
    data: qs.stringify({
      Account,
      Password
    })
  })
}

export function logout() {
  return fetch({
    url: '/system/loginOut',
    method: 'get'
  })
}

export function getUserInfo(data) {
  return fetch({
    url: '/system/getUserInfo',
    method: 'get',
    params: data
  })
}

