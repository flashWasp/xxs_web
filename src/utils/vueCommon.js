import { toDataStr, toDateTimeStr } from './index'
import fetch from '@/utils/fetch.js'
import newsModuleFetch from '@/utils/fetch/newsModule' // 新闻模块请求封装
import employeeFetch from '@/utils/fetch/employee' // 员工模块请求封装
import companyFetch from '@/utils/fetch/company' // 公司模块请求封装
import qs from 'qs'
const defaultSize = 10

export function pageUtilsInstall(Vue) {
  Vue.prototype.$validate = function(formName) {
    return new Promise((resolve, reject) => {
      this.$refs[formName].validate((rs) => {
        if (rs) {
          resolve(rs)
        }
      })
    })
  }

  Vue.prototype.$resetFields = function(formName) {
    return new Promise((resolve, reject) => {
      this.$nextTick(() => {
        this.$refs[formName].resetFields()
        resolve()
      })
    })
  }

  Vue.prototype.$myConfirm = function(title, msg = '提示', config = {
    confirmButtonText: '确定',
    cancelButtonText: '取消',
    type: 'warning'
  }) {
    return new Promise((resolve, reject) => {
      this.$confirm(title, msg, config).then(() => {
        resolve(true)
      }).catch(() => {
        // resolve(false)
      })
    })
  }

  Vue.prototype.$toDataStr = function(val, fmt) {
    return toDataStr(val, fmt)
  }

  Vue.prototype.$toDateTimeStr = function(val, fmt) {
    return toDateTimeStr(val, fmt)
  }

  Vue.prototype.$has = function(code) {
    const buttonMap = this.$store.getters.buttonMap || {}
    if (!code || buttonMap[code]) {
      return true
    }
    return false
  }

  //  图片服务
  Vue.prototype.$imageBaseApi = function() {
    return process.env.NODE_ENV === 'development' ? '/img-server/api' : '/img-server'
  }
  //  新闻模块  ===========       start    ===========
  Vue.prototype.$newsModuleBaseApi = function() {
    return process.env.NODE_ENV === 'development' ? '/xxs-news-server/api' : '/xxs-news-server'
  }
  Vue.prototype.$newsModuleHttp = {
    get: function(url, params) {
      return newsModuleFetch({
        url: url,
        method: 'get',
        params
      })
    },
    post: function(url, params) {
      return newsModuleFetch({
        url: url,
        method: 'post',
        // data: qs.stringify(params)
        data: params
      })
    }
  }
  //          ==========        end       ===========
  //  内部员工模块  ===========       start    ===========
  Vue.prototype.$employeeBaseApi = function() {
    return process.env.NODE_ENV === 'development' ? '/xxs-user-server/api' : '/xxs-user-server'
  }
  Vue.prototype.$employeeHttp = {
    get: function(url, params) {
      return employeeFetch({
        url: url,
        method: 'get',
        params
      })
    },
    post: function(url, params) {
      return employeeFetch({
        url: url,
        method: 'post',
        // data: qs.stringify(params)
        data: params
      })
    }
  }
  //          ==========        end       ===========

  //  公司模块  ===========       start    ===========
  Vue.prototype.$companyBaseApi = function() {
    return process.env.NODE_ENV === 'development' ? '/xxs-company-server/api' : '/xxs-company-server'
  }
  Vue.prototype.$companyHttp = {
    get: function(url, params) {
      return companyFetch({
        url: url,
        method: 'get',
        params
      })
    },
    post: function(url, params) {
      return companyFetch({
        url: url,
        method: 'post',
        // data: qs.stringify(params)
        data: params
      })
    }
  }
  //          ==========        end       ===========
  Vue.prototype.$baseApi = function() {
    return process.env.BASE_API
  }

  Vue.prototype.$http = {
    get: function(url, params) {
      return fetch({
        url: url,
        method: 'get',
        params
      })
    },
    post: function(url, params) {
      return fetch({
        url,
        method: 'post',
        data: qs.stringify(params)
      })
    }
  }

  Vue.prototype.$msg = function(response) {
    if (+response.data.statusCode !== 200) {
      if (response.data.message) {
        this.$notify.error({
          title: '错误',
          message: response.data.message,
          duration: 4000
        })
      }
    } else {
      this.$notify({
        title: '成功',
        message: response.data.message,
        type: 'success',
        duration: 4000
      })
    }
  }

  Vue.prototype.$notifySuccess = function(message = '成功', title = '提示', duration = 2000) {
    this.$notify.success({
      title: title,
      message: message,
      duration: duration
    })
  }

  Vue.prototype.$notifyError = function(message = '失败', title = '提示', duration = 2000) {
    this.$notify.error({
      title: title,
      message: message,
      duration: duration
    })
  }

  Vue.prototype.$tip = function(msg) {
    this.$message.warning(msg)
  }

  Vue.prototype.$toQueryString = function(params) {
    return qs.stringify(params)
  }

  // 自定义提示 出现在页面右侧 顶部会挡住看不见 start
  Vue.prototype.$customTip = function(msg) {
    this.$notify({
      title: '提示',
      message: msg,
      type: 'warning',
      offset: 100
    })
  }

  // Vue.prototype.$customMsg = function(response) {
  //   if (response.data.code < 0 || response.data.code === undefined) {
  //     if (response.data.message) {
  //       this.$notify.error({
  //         title: '错误',
  //         message: response.data.message,
  //         duration: 2000,
  //         offset: 100
  //       })
  //     }
  //   } else {
  //     this.$notify({
  //       title: '成功',
  //       message: response.data.message,
  //       type: 'success',
  //       duration: 2000,
  //       offset: 100
  //     })
  //   }
  // }
  // end

  // Vue.prototype.$getCode = Vue.$getCode = async function(params) {
  //   const code = params.join ? params.join(',') : params
  //   const response = await fetch({
  //     url: '/dict/code',
  //     method: 'get',
  //     params: { code }
  //   })
  //   return response.data
  // }

  const codeMap = {}
  Vue.prototype.$initCode = async function(code) {
    const codeArray = code.push ? code : [code]
    const requestCode = []
    codeArray.forEach(c => {
      if (!codeMap[c]) {
        requestCode.push(c)
      }
    })
    if (requestCode.length > 0) {
      const rsCodeMap = await getCode(requestCode)
      for (const key in rsCodeMap) {
        codeMap[key] = rsCodeMap[key]
      }
    }
  }

  Vue.prototype.$getCodeMap = function(code) {
    const list = codeMap[code] || []
    const map = {}
    list.forEach(item => {
      map[item.code] = item.fullName
    })
    return map
  }

  const codeToKVMap = {}
  Vue.prototype.$v = function(code, val) {
    let map = codeToKVMap[code]
    if (!map) {
      map = {}
      const list = codeMap[code] || []
      list.forEach(item => {
        map[item.code] = item.fullName
      })
    }
    return map[val] || val
  }

  Vue.filter('v', function(val, code) {
    let map = codeToKVMap[code]
    if (!map) {
      map = {}
      const list = codeMap[code] || []
      list.forEach(item => {
        map[item.code] = item.fullName
      })
    }
    return map[val] || val
  })

  Vue.prototype.$getCodeList = function(code) {
    return codeMap[code] || []
  }

  Vue.prototype.$treeHeight = function(code) {
    return document.documentElement.clientHeight - 150
  }

  /* whc, 价格转换，把分单位价格转换成元 */
  Vue.prototype.$convertMoney = function(cents) {
    if (isNaN(cents)) {
      return ''
    }
    return parseFloat(cents * 0.01 + '').toFixed(2)
  }

  // 回到顶部
  Vue.prototype.$backToTop = function() {
    const start = window.pageYOffset
    let i = 0
    const backPosition = 0
    this.interval = setInterval(() => {
      let t = 10 * i
      let num = start / 2 * (--t * (t - 2) - 1) + start
      if ((t /= 500 / 2) < 1) {
        num = start / 2 * 10 * i * 10 * i - start
      }
      const next = Math.floor(num)
      if (next <= backPosition) {
        window.scrollTo(0, backPosition)
        clearInterval(this.interval)
      } else {
        window.scrollTo(0, next)
      }
      i++
    }, 16.7)
  }

  /* 是否为数组，是返回原值，否返回null */
  Vue.prototype.$isNumber = function(value) {
    if (isNaN(value)) {
      return null
    }
    return value
  }

  /* 是否为数组，是返回原值，否返回null */
  Vue.prototype.$isInteger = function(value) {
    const num = parseInt(value + '')
    if (isNaN(num)) {
      return null
    }
    return num
  }

  /* 是否为数组，是返回原值，否返回null */
  Vue.prototype.$isFloat = function(value) {
    const num = parseFloat(value + '')
    if (isNaN(num)) {
      return null
    }
    return num
  }

  /* end */

  Vue.mixin({
    data() {
      return {
        pageParams: {
          sortName: null,
          sortOrder: null
        }
      }
    },
    methods: {
      $setPage(val) {
        if (window.localStorage) {
          const pageSizes = JSON.parse(window.localStorage.pageSizes || '{}') || {}
          pageSizes[this.$route.path] = val
          window.localStorage.pageSizes = JSON.stringify(pageSizes)
        }
      },
      $getPage(val) {
        if (window.localStorage) {
          const pageSizes = JSON.parse(window.localStorage.pageSizes || '{}') || {}
          return pageSizes[this.$route.path] || defaultSize
        }
        return defaultSize
      },
      $handleSizeChange(val) {
        this.$setPage(val)
        this.pageSize = val
        this.getTableList()
      },
      $handleCurrentChange(val) {
        this.currentPage = val
        this.getTableList()
      },
      $handleFilter() {
        this.currentPage = 1
        this.getTableList()
      },
      /* 营销系统接待员页面用  start */
      $handleSizeChangeBySalesman(val) {
        this.$setPage(val)
        this.pageSizeBySalesman = val
        this.getSalesManData()
      },
      $handleCurrentChangeBySalesman(val) {
        this.currentPageBySalesman = val
        this.getSalesManData()
      },
      $handleFilterBySalesman() {
        this.currentPageBySalesman = 1
        this.getSalesManData()
      },
      /* end */
      /* 营销系统  用户领取优惠券列表页面用 start */
      $handleSizeChangeByUserCoupon(val) {
        this.$setPage(val)
        this.pageSizeByUserCoupon = val
        this.getUserCouponData()
      },
      $handleCurrentChangeByUserCoupon(val) {
        this.currentPageByUserCoupon = val
        this.getUserCouponData()
      },
      $handleFilterByUserCoupon() {
        this.currentPageByUserCoupon = 1
        this.getUserCouponData()
      },
      /* end */
      /* 营销系统  线上抢房加载选择房屋页面用 start */
      $handleSizeChangeByAllHouse(val) {
        this.$setPage(val)
        this.pageSizeByAllHouse = val
        this.changePageCoreRecordData()
        this.getHouseList()
      },
      $handleCurrentChangeByAllHouse(val) {
        this.currentPageByAllHouse = val
        this.changePageCoreRecordData()
        this.getHouseList()
      },
      $handleFilterByAllHouse() {
        this.currentPageByAllHouse = 1
        this.changePageCoreRecordData()
        this.getHouseList()
      },
      /* end */
      $sortChange(params) {
        if (params.prop && params.order) {
          this.pageParams.sortName = params.prop
          this.pageParams.sortOrder = params.order === 'ascending' ? 'asc' : 'desc'
        } else {
          this.pageParams.sortName = null
          this.pageParams.sortOrder = null
        }

        this.getTableList && this.getTableList()
      }
    }
  })
}

async function getCode(params) {
  const code = params.join ? params.join(',') : params
  const response = await fetch({
    url: '/dict/code',
    method: 'get',
    params: { code }
  })
  return response.data
}
