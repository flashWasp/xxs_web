import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/views/layout/Layout'

/** note: submenu only apppear when children.length>=1
*   detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
**/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']     will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
    noCache: true                if true ,the page will no be cached(default is false)
  }
**/
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/authredirect', component: () => import('@/views/login/authredirect'), hidden: true },
  { path: '/404', component: () => import('@/views/errorPage/404'), hidden: true },
  { path: '/401', component: () => import('@/views/errorPage/401'), hidden: true },
  {
    path: '/',
    component: Layout,
    redirect: 'dashboard',
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      name: '首页',
      meta: { title: '首页', icon: 'dashboard', noCache: true }
    }]
  },
  {
    path: '/article',
    component: Layout,
    redirect: 'noredirect',
    name: '文章管理',
    meta: {
      title: '文章管理',
      icon: 'fa fa-book'
    },
    children: [
      { path: 'newsChannel', component: () => import('@/views/article/newsChannel'), name: '频道管理', meta: { title: '频道管理' }},
      { path: 'news', component: () => import('@/views/article/news'), name: '文章列表', meta: { title: '文章列表' }},
      { path: 'editArticle', component: () => import('@/views/article/editArticle'), name: '编辑文章', meta: { title: '编辑文章' }, hidden: true },
      { path: 'hotArticle', component: () => import('@/views/article/hotArticle'), name: '热门文章', meta: { title: '热门文章' }},
      { path: 'comment', component: () => import('@/views/article/comment'), name: '评论列表', meta: { title: '评论列表' }}
    ]
  },
  {
    path: '/company',
    component: Layout,
    redirect: 'noredirect',
    name: '公司管理',
    meta: {
      title: '公司管理',
      icon: 'fa fa-institution'
    },
    children: [
      { path: 'companySort', component: () => import('@/views/company/companySort'), name: '公司分类', meta: { title: '公司分类' }},
      { path: 'companyRegion', component: () => import('@/views/company/companyRegion'), name: '公司区域', meta: { title: '公司区域' }},
      { path: 'companyBase', component: () => import('@/views/company/companyBase'), name: '公司信息', meta: { title: '公司信息' }}
    ]
  },
  {
    path: '/hotDiscuss',
    component: Layout,
    redirect: 'noredirect',
    name: '热门讨论',
    meta: {
      title: '热门讨论',
      icon: 'fa fa-comment-o'
    },
    children: [
      { path: 'siftTheme', component: () => import('@/views/hotDiscuss/siftTheme'), name: '精选主题', meta: { title: '精选主题' }},
      { path: 'discussList', component: () => import('@/views/hotDiscuss/discussList'), name: '讨论列表', meta: { title: '讨论列表' }}
    ]
  },
  {
    path: '/coquette',
    component: Layout,
    redirect: 'noredirect',
    name: '风情景观',
    meta: {
      title: '风情景观',
      icon: 'fa fa-camera-retro'
    },
    children: [
      { path: 'town', component: () => import('@/views/coquette/town'), name: '特色小镇', meta: { title: '特色小镇', noCache: true }},
      { path: 'store', component: () => import('@/views/coquette/store'), name: '店铺优选', meta: { title: '店铺优选', noCache: true }}
    ]
  },
  {
    path: '/platform',
    component: Layout,
    redirect: 'noredirect',
    name: '平台管理',
    meta: {
      title: '平台管理',
      icon: 'fa fa-navicon'
    },
    children: [
      { path: 'platformUser', component: () => import('@/views/platform/platformUser'), name: '员工管理', meta: { title: '员工管理' }},
      { path: 'role', component: () => import('@/views/platform/role'), name: '角色管理', meta: { title: '角色管理' }},
      { path: 'customerService', component: () => import('@/views/platform/customerService'), name: '客服设置', meta: { title: '客服设置' }}
    ]
  },
  {
    path: '/sysUser',
    component: Layout,
    redirect: 'noredirect',
    name: '用户体系',
    meta: {
      title: '用户体系',
      icon: 'fa fa-address-book-o'
    },
    children: [
      { path: 'userList', component: () => import('@/views/sysUser/userList'), name: '用户列表', meta: { title: '用户列表', noCache: true }},
      { path: 'userMember', component: () => import('@/views/sysUser/userMember'), name: '会员列表', meta: { title: '会员列表', noCache: true }},
      { path: 'operateLog', component: () => import('@/views/sysUser/operateLog'), name: '用户操作日志', meta: { title: '用户操作日志', noCache: true }},
      { path: 'runAccount', component: () => import('@/views/sysUser/runAccount'), name: '会员交易流水', meta: { title: '会员交易流水', noCache: true }}
    ]
  },
  {
    path: '/operation',
    component: Layout,
    redirect: 'noredirect',
    name: '运营管理',
    meta: {
      title: '运营管理',
      icon: 'fa fa-sitemap'
    },
    children: [
      { path: 'teamworkAd', component: () => import('@/views/operation/teamworkAd'), name: '合作广告', meta: { title: '合作广告', noCache: true }},
      { path: 'friendLink', component: () => import('@/views/operation/friendLink'), name: '友情链接', meta: { title: '友情链接', noCache: true }},
      { path: 'generalize', component: () => import('@/views/operation/generalize'), name: '店铺推广', meta: { title: '店铺推广', noCache: true }}
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [
  // {
  //   path: '/article',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   name: '文章管理',
  //   meta: {
  //     title: '文章管理',
  //     icon: 'list'
  //   },
  //   children: [
  //     { path: 'news', component: () => import('@/views/article/news'), name: '新闻列表', meta: { title: '新闻列表' }},
  //     { path: 'hotArticle', component: () => import('@/views/article/hotArticle'), name: '热门文章', meta: { title: '热门文章' }},
  //     { path: 'comment', component: () => import('@/views/article/comment'), name: '评论列表', meta: { title: '评论列表' }}
  //   ]
  // },
  // {
  //   path: '/hotDiscuss',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   name: '热门讨论',
  //   meta: {
  //     title: '热门讨论',
  //     icon: 'component'
  //   },
  //   children: [
  //     { path: 'siftTheme', component: () => import('@/views/hotDiscuss/siftTheme'), name: '精选主题', meta: { title: '精选主题' }},
  //     { path: 'discussList', component: () => import('@/views/hotDiscuss/discussList'), name: '讨论列表', meta: { title: '讨论列表' }}
  //   ]
  // },
  // {
  //   path: '/coquette',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   name: '风情景观',
  //   meta: {
  //     title: '风情景观',
  //     icon: 'chart'
  //   },
  //   children: [
  //     { path: 'town', component: () => import('@/views/coquette/town'), name: '特色小镇', meta: { title: '特色小镇', noCache: true }},
  //     { path: 'store', component: () => import('@/views/coquette/store'), name: '店铺优选', meta: { title: '店铺优选', noCache: true }}
  //   ]
  // },
  // {
  //   path: '/platform',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   name: '平台管理',
  //   meta: {
  //     title: '平台管理',
  //     icon: 'table'
  //   },
  //   children: [
  //     { path: 'platformUser', component: () => import('@/views/platform/platformUser'), name: '平台用户', meta: { title: '平台用户' }},
  //     { path: 'role', component: () => import('@/views/platform/role'), name: '角色权限', meta: { title: '角色权限' }},
  //     { path: 'customerService', component: () => import('@/views/platform/customerService'), name: '客服设置', meta: { title: '客服设置' }}
  //   ]
  // },
  // {
  //   path: '/sysUser',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   name: '用户体系',
  //   meta: {
  //     title: '用户体系',
  //     icon: 'peoples'
  //   },
  //   children: [
  //     { path: 'userList', component: () => import('@/views/sysUser/userList'), name: '用户列表', meta: { title: '用户列表', noCache: true }},
  //     { path: 'userMember', component: () => import('@/views/sysUser/userMember'), name: '会员列表', meta: { title: '会员列表', noCache: true }},
  //     { path: 'operateLog', component: () => import('@/views/sysUser/operateLog'), name: '用户操作日志', meta: { title: '用户操作日志', noCache: true }},
  //     { path: 'runAccount', component: () => import('@/views/sysUser/runAccount'), name: '会员交易流水', meta: { title: '会员交易流水', noCache: true }}
  //   ]
  // },
  // {
  //   path: '/operation',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   name: '运营管理',
  //   meta: {
  //     title: '运营管理',
  //     icon: 'example'
  //   },
  //   children: [
  //     { path: 'teamworkAd', component: () => import('@/views/operation/teamworkAd'), name: '合作广告', meta: { title: '合作广告', noCache: true }},
  //     { path: 'friendLink', component: () => import('@/views/operation/friendLink'), name: '友情链接', meta: { title: '友情链接', noCache: true }},
  //     { path: 'generalize', component: () => import('@/views/operation/generalize'), name: '店铺推广', meta: { title: '店铺推广', noCache: true }}
  //   ]
  // },
  // {
  //   path: '/i18n',
  //   component: Layout,
  //   children: [{ path: 'index', component: () => import('@/views/i18n-demo/index'), name: 'i18n', meta: { title: 'i18n', icon: 'international' }}]
  // },

  { path: '*', redirect: '/404', hidden: true }
]
